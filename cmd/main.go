package main

import (
	"context"

	"gitlab.com/serkurnikov/uni-exchange/internal/interface/cli"
)

func main() {
	cliApp := cli.NewApp()
	cliApp.Run(context.Background())
}
