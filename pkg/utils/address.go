package utils

import "github.com/ethereum/go-ethereum/common"

func CompareEthAddresses(addr1, addr2 string) bool {
	return common.HexToAddress(addr1) == common.HexToAddress(addr2)
}

func ContainsTokens(fromToken, toToken string, contractToken0, contractToken1 common.Address) bool {
	from := common.HexToAddress(fromToken)
	to := common.HexToAddress(toToken)

	return (from == contractToken0 && to == contractToken1) || (from == contractToken1 && to == contractToken0)
}
