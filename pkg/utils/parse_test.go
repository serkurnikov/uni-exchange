package utils

import "testing"

func TestScientificNotationToBigInt(t *testing.T) {
	testCases := []struct {
		name          string
		sciNotation   string
		expectedValue string // This is a string for easy comparison
		expectError   bool
	}{
		{"ValidConversion1", "1e18", "1000000000000000000", false},
		{"ValidConversion2", "5e10", "50000000000", false},
		{"InvalidBase", "1x18", "", true},
		{"InvalidExponent", "1e1x8", "", true},
		{"NonScientific", "25000040000500000700000800000", "25000040000500000700000800000", false},
		{"InvalidFormat", "1000.1000", "", true},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, err := ParseStringToBigInt(tc.sciNotation)

			if (err != nil) != tc.expectError {
				t.Errorf("Expected error: %v, got: %v", tc.expectError, err)
			}

			if !tc.expectError && result.String() != tc.expectedValue {
				t.Errorf("Expected %s, got %s", tc.expectedValue, result.String())
			}
		})
	}
}
