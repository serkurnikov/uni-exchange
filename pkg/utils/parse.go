package utils

import (
	"fmt"
	"math/big"
	"strconv"
	"strings"
)

const (
	base10     = 10
	partsCount = 2
)

// ParseStringToBigInt analyzes a string and converts it into a *big.Int.
// It supports both scientific notation and regular numeric strings.
func ParseStringToBigInt(str string) (*big.Int, error) {
	parts := strings.Split(str, "e")

	var decimal *big.Int
	var err error

	switch len(parts) {
	case 1: // Handling regular numbers
		decimal, err = handleRegularNumbers(str)
	case partsCount: // Handling scientific notation
		decimal, err = handleScientificNotation(parts[0], parts[1])
	default:
		err = fmt.Errorf("invalid number format: %s", str)
	}

	if err != nil {
		return nil, err
	}

	// Check if the value is negative
	if decimal.Sign() < 0 {
		return nil, fmt.Errorf("negative values are not allowed: %s", str)
	}

	return decimal, nil
}

func handleRegularNumbers(str string) (*big.Int, error) {
	decimal, ok := new(big.Int).SetString(str, base10)
	if !ok {
		return nil, fmt.Errorf("invalid number: %s", str)
	}

	return decimal, nil
}

func handleScientificNotation(baseStr, expStr string) (*big.Int, error) {
	base, err := strconv.ParseInt(baseStr, base10, 64)
	if err != nil {
		return nil, fmt.Errorf("invalid base in scientific notation: %s", baseStr)
	}
	exponent, err := strconv.ParseInt(expStr, base10, 64)
	if err != nil {
		return nil, fmt.Errorf("invalid exponent in scientific notation: %s", expStr)
	}
	decimal := big.NewInt(base)
	multiplier := new(big.Int).Exp(big.NewInt(base10), big.NewInt(exponent), nil)
	decimal.Mul(decimal, multiplier)

	return decimal, nil
}
