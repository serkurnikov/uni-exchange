# Uniswap V2 Calculator CLI

## Overview
This Command-Line Interface (CLI) application calculates the expected output amount for a token swap in a Uniswap V2 pool on the Ethereum blockchain. It queries real-time reserve data from Uniswap V2 pools and computes output amounts, taking into account Uniswap's trading mechanics and fees.

### Structure of Go packages

- `cmd/*` - Contains the main applications of the project. Each subdirectory represents a distinct executable application.
- `internal/infrastructure` - Houses the infrastructure-related code, including server configurations and handlers for various services.
    - `contracts` - Implements handlers for interacting with smart contracts, potentially for Uniswap V2 or other blockchain-based contracts.
    - `ethclient` - Provides handlers for Ethereum client interactions, such as querying blockchain data or sending transactions.
- `interface/*` - Defines interfaces for the domain layer
    - `cli` - Contains CLI (Command Line Interface) implementations, facilitating user interaction with the application through the command line.
- `internal/usecase/*` - Implements the use cases of the application, focusing on business logic. For example, a Uniswap V2 calculator for swap calculations.
- `pkg/*` - Includes helper packages that are not directly tied to the application's core architecture or business logic. These packages provide utility functions and might be externalized in the future.
  replaced by external dependencies), e.g.:
    - `cache/` - Contains functions for cache management, including cache keys generation and handling.
    - `utils/` - Provides utility functions, such as those for checking and comparing Ethereum addresses, which are essential for blockchain-related operations.

## Features

- Fetches real-time reserve data from Uniswap V2 pools.
- Computes the swap output amount based on the current pool state.
- Accepts input amounts in scientific notation.
- Connects to the Ethereum blockchain using Infura.

## Prerequisites

- Go (Golang) installed.
- An active Infura account with an Ethereum mainnet API key.

## Installation

**Clone the repository:**
```bash
   git clone https://gitlab.com/serkurnikov/uni-exchange.git
   cd uni-exchange/cmd
   go run main.go
   ```

**Usage**

To use the application, you need to pass the Uniswap V2 pool address, input and output token addresses, and the input amount. 
The application will output the calculated amount of the output token.

- Input params
```bash
PoolID: 0x0d4a11d5eeaac28ec3f61d100daf4d40471f1852
FromToken: 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2
ToToken: 0xdac17f958d2ee523a2206206994597c13d831ec7
InputAmount: 1e18
```

- Run programming
```bash
Enter parameters in format: <PoolID> <FromToken> <ToToken> <InputAmount>
Enter parameters: 0x0d4a11d5eeaac28ec3f61d100daf4d40471f1852 0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2 0xdac17f958d2ee523a2206206994597c13d831ec7 1e18
Output Amount: 2039001245
```

## Calculating Output Amount in Uniswap V2

### Overview

Uniswap V2 utilizes an automated market maker (AMM) protocol to facilitate token swaps. It employs a constant product formula to determine the amount of output tokens (`outputAmount`) for a given input token amount (`inputAmount`). Understanding this formula is essential for predicting swap outcomes and integrating with Uniswap V2 pools.

### Constant Product Formula

At the core of Uniswap V2's AMM model is the constant product formula: ```x * y = k```

Where:
- `x` and `y` are the reserves of the two tokens in the liquidity pool.
- `k` is a constant value.

During a swap, the product of the reserves (`k`) must remain constant. As a result, when the amount of one token in the pool increases, the amount of the other decreases proportionally.

### Swap Formula

![asd](https://docs.uniswap.org/assets/images/trade-b19a05be2c43a62708ab498766dc6d13.jpg)

The formula to calculate the output amount in a Uniswap V2 swap, considering the standard 0.3% fee, is:

```
outputAmount = (inputAmount * 997 / 1000 * reserveOutput) / (reserveInput + inputAmount * 997 / 1000)
```

### Parameters:
- `inputAmount`: The amount of the input token being swapped.
- `reserveInput`: The current reserve of the input token in the pool.
- `reserveOutput`: The current reserve of the output token in the pool.

### Example Calculation:

#### Context
- **Pair Address**: `0x0d4a11d5eeaac28ec3f61d100daf4d40471f1852`
- **Ticker**: `WETH-USDT`

#### Tokens Details
1. **Wrapped Ether (WETH)**
  - Address: `0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2`
  - Symbol: "WETH"
  - PriceUSD: 2068.7123183341605,

2. **Tether USD (USDT)**
  - Address: `0xdac17f958d2ee523a2206206994597c13d831ec7`
  - Symbol: "USDT"
  - PriceUSD: 1.000562899837469,

#### Reserves
- `reserve0`: `20866.116582865415956206 WETH`
- `reserve1`: `42684732.657357 USDT`

#### Calculation with Decimal
- Input Amount: `0.6288854022386539 WETH`

- Output Amount Calculation:
```
outputAmount = (0.6288854022386539 * 997 / 1000 * 42684732.657357) /
(20866.116582865415956206 + 0.6288854022386539 * 997 / 1000)
= 1282.5802812115 USDT
```


#### Calculation with BigInt (Without Adjusting for Decimals)
GetReserves Method Response:
- `reserve0`: `20866116582865415956206`
- `reserve1`: `42684732657357`

Example Calculation
- Input Amount: `0.6288854022386539 WETH`
- Represented as `6.288854022386539e17` in base units (since WETH has 18 decimals)

- Output Amount Calculation:
```
outputAmount = (6.288854022386539e17 * 997 / 1000 * 42684732657357) /
(20866116582865415956206 + 6.288854022386539e17 * 997 / 1000)
= 1282580281.211494
```

#### In Base Units
- Same calculation in base units for USDT (with 6 decimals):

### Summary
These calculations demonstrate how output amounts can be computed in both `Decimal` and `BigInt` formats, without adjusting for the number of decimals in the input or output tokens. The approach varies in data representation, but the underlying mathematical process remains the same.
