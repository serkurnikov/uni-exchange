PROJECT_NAME := "uni-exchange"
PKG := $(PROJECT_NAME)
PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: lint test dep help

lint: ## Lint testing
	@go fmt ./...
	@golangci-lint run -v

test: ## Generate global code coverage report
	@go test ./... -coverprofile=coverage.out -coverpkg=./...
	@go tool cover -html=coverage.out -o cover.html
	@rm -rf ./coverage.out

dep: ## Get the dependencies
	@go get -v -d ./...
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'