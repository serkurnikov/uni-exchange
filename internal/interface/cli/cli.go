package cli

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	"gitlab.com/serkurnikov/uni-exchange/internal/infrastructure/ethclient"
	"gitlab.com/serkurnikov/uni-exchange/internal/usecase"
	"gitlab.com/serkurnikov/uni-exchange/pkg/utils"
)

const (
	apiInfuraKey = "9a3d25e0db044a51a727f24913615fed"

	totalCountParams = 4
)

type App struct {
	calculator usecase.Calculator
	client     *ethclient.Client
}

func NewApp() *App {
	return &App{
		client:     ethclient.NewClient("https://mainnet.infura.io/v3/" + apiInfuraKey),
		calculator: usecase.NewCalculator(),
	}
}

func (app *App) Run(ctx context.Context) {
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("Enter parameters in format: <PoolID> <FromToken> <ToToken> <InputAmount>")

	for {
		fmt.Print("Enter parameters: ")
		input, err := reader.ReadString('\n')
		if err != nil {
			log.Printf("Error reading input: %v", err)
			continue
		}

		input = strings.TrimSpace(input)
		params := strings.Split(input, " ")

		if len(params) != totalCountParams {
			fmt.Println("Invalid input format. Please use format: <PoolID> <FromToken> <ToToken> <InputAmount>")
			continue
		}

		poolID := params[0]
		fromToken := params[1]
		toToken := params[2]
		inputAmountStr := params[3]

		fmt.Printf("Input amount base units: %s\n", inputAmountStr)
		amount, err := utils.ParseStringToBigInt(inputAmountStr)
		if err != nil {
			log.Printf("Error parsing input amount: %v\n", err)
			continue
		}

		reserveInput, reserveOutput, err := app.client.GetReserves(ctx, ethclient.GetReservesReq{
			PairAddress: poolID,
			Token0:      fromToken,
			Token1:      toToken,
			InputAmount: amount,
		})
		if err != nil {
			log.Printf("Failed to get reserves: %v\n", err)

			continue
		}

		outputAmount, err := app.calculator.CalculateOutputAmount(amount, reserveInput, reserveOutput)
		if err != nil {
			log.Printf("Error calculating output amount: %v", err)
			continue
		}
		fmt.Printf("Output amount base units: %s\n", outputAmount)
	}
}
