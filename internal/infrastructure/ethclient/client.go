package ethclient

import (
	"context"
	"fmt"
	"log"
	"math/big"

	"github.com/dgraph-io/ristretto"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/serkurnikov/uni-exchange/internal/infrastructure/contracts/uniswapv2"
	"gitlab.com/serkurnikov/uni-exchange/pkg/cache"
	"gitlab.com/serkurnikov/uni-exchange/pkg/utils"
)

type Client struct {
	ethClient *ethclient.Client
	cache     *ristretto.Cache
}

func NewClient(url string) *Client {
	eth, err := ethclient.Dial(url)
	if err != nil {
		log.Fatalf("failed init ethclient: %v", err)
	}

	ristrettoCache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,
		MaxCost:     1 << 30,
		BufferItems: 64,
	})
	if err != nil {
		log.Fatalf("failed init cache: %v", err)
	}

	return &Client{ethClient: eth, cache: ristrettoCache}
}

func (c *Client) GetReserves(ctx context.Context, req GetReservesReq) (reserve0, reserve1 *big.Int, err error) {
	instance, err := c.getPairInstance(req.PairAddress)
	if err != nil {
		return zeroValue, zeroValue, err
	}

	reserves, err := instance.GetReserves(&bind.CallOpts{Context: ctx})
	if err != nil {
		return zeroValue, zeroValue, fmt.Errorf("failed to get reserves: %w", err)
	}

	contractToken0, err := instance.Token0(&bind.CallOpts{Context: ctx})
	if err != nil {
		return zeroValue, zeroValue, fmt.Errorf("failed to get token0: %w", err)
	}

	contractToken1, err := instance.Token1(&bind.CallOpts{Context: ctx})
	if err != nil {
		return zeroValue, zeroValue, fmt.Errorf("failed to get token1: %w", err)
	}

	if !utils.ContainsTokens(req.Token0, req.Token1, contractToken0, contractToken1) {
		return zeroValue, zeroValue, fmt.Errorf("token addresses do not match contract tokens")
	}

	var reserveToCheck *big.Int
	var inverseOrder bool // false for (Reserve0, Reserve1), true for (Reserve1, Reserve0)

	if utils.CompareEthAddresses(req.Token0, contractToken0.Hex()) {
		reserveToCheck = reserves.Reserve0
		inverseOrder = false
	} else {
		reserveToCheck = reserves.Reserve1
		inverseOrder = true
	}

	if req.InputAmount != nil && req.InputAmount.Cmp(reserveToCheck) > 0 {
		return zeroValue, zeroValue, fmt.Errorf("insufficient reserve for the input amount: max available is %v", reserveToCheck)
	}

	if inverseOrder {
		return reserves.Reserve1, reserves.Reserve0, nil
	}
	return reserves.Reserve0, reserves.Reserve1, nil
}

func (c *Client) getPairInstance(address string) (*uniswapv2.Uniswapv2, error) {
	key := cache.PairCacheKey(address)
	if data, ok := c.cache.Get(key); ok {
		return data.(*uniswapv2.Uniswapv2), nil
	}

	instance, err := uniswapv2.NewUniswapv2(common.HexToAddress(address), c.ethClient)
	if err != nil {
		return nil, fmt.Errorf("failed to create Uniswap V2 instance: %w", err)
	}

	c.cache.SetWithTTL(key, instance, 0, cacheTTL)
	c.cache.Wait()
	return instance, nil
}
