package ethclient

import (
	"math/big"
	"time"
)

var (
	cacheTTL = time.Hour

	zeroValue = new(big.Int).SetUint64(0)
)

type GetReservesReq struct {
	PairAddress string
	Token0      string
	Token1      string
	InputAmount *big.Int
}
