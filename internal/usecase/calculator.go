package usecase

import (
	"fmt"
	"math/big"
)

const (
	// UniswapFeeNumerator Default uniswap fee (0.3%)
	UniswapFeeNumerator   int64 = 997
	UniswapFeeDenominator int64 = 1000
)

type Calculator struct {
}

func NewCalculator() Calculator {
	return Calculator{}
}

func (c *Calculator) CalculateOutputAmount(inputAmount, reserveInput, reserveOutput *big.Int) (*big.Int, error) {
	// Application of commission (0.3%)
	fee := big.NewInt(UniswapFeeNumerator)
	inputAmountWithFee := new(big.Int).Mul(inputAmount, fee)

	// Calculate the numerator and denominator to calculate the exchange volume
	numerator := new(big.Int).Mul(inputAmountWithFee, reserveOutput)
	denominator := new(big.Int).Mul(reserveInput, big.NewInt(UniswapFeeDenominator))
	denominator.Add(denominator, inputAmountWithFee)

	if denominator.Cmp(big.NewInt(0)) == 0 {
		return nil, fmt.Errorf("failed CalculateOutputAmount: denominator can't be 0")
	}

	return new(big.Int).Div(numerator, denominator), nil
}
