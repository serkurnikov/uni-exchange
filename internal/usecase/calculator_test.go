package usecase

import (
	"math/big"
	"testing"
)

func TestCalculateOutputAmount(t *testing.T) {
	testCases := []struct {
		name           string
		inputAmount    *big.Int
		reserveInput   *big.Int
		reserveOutput  *big.Int
		expectedOutput string
	}{
		{"BasicSwap", big.NewInt(1000), big.NewInt(10000), big.NewInt(5000), "453"},
	}

	calculator := NewCalculator()

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			result, err := calculator.CalculateOutputAmount(tc.inputAmount, tc.reserveInput, tc.reserveOutput)
			if err != nil {
				t.Errorf("Error should be empty: %s", err.Error())
			}

			if result.String() != tc.expectedOutput {
				t.Errorf("Expected %s, got %s", tc.expectedOutput, result.String())
			}
		})
	}
}
